from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'blacknwhite.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^black/', include('black.urls', namespace='black')),
    url(r'^', include('white.urls', namespace='white')),
    url(r'^accounts/', include('accounts.urls')),
)
