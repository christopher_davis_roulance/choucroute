from django import forms
from django.utils.text import slugify
from white.models import Project, Section
from accounts.models import CustomUser


class BaseForm(forms.ModelForm):

    class Meta:
        exclude = ['created_by', 'modified_by', 'slug', 'workflow_state', ]


class ProjectForm(BaseForm):
    section = forms.ModelChoiceField(queryset=Section.objects.filter(is_primary=True))
    architects = forms.ModelMultipleChoiceField(queryset=CustomUser.objects.filter(user_typology=10))
    clients = forms.ModelMultipleChoiceField(queryset=CustomUser.objects.filter(user_typology=20))
    contributors = forms.ModelMultipleChoiceField(queryset=CustomUser.objects.filter(user_typology=30))

    class Meta(BaseForm.Meta):
        model = Project

    def save(self, commit=True, *args, **kwargs):
        user = kwargs.pop('current_user')
        obj = super(ProjectForm, self).save(commit=False)
        if not obj.pk:
            obj.created_by = user
            obj.modified_by = user
        else:
            obj.modifier_by = user
        obj.slug = u'{0},{1}'.format(slugify(obj.title), obj.id)
        if commit:
            obj.save()


class SectionForm(BaseForm):

    primary_section = forms.ModelChoiceField(queryset=Section.objects.filter(is_primary=True), required=False)

    class Meta(BaseForm.Meta):
        model = Section

    def __init__(self, current_user, *args, **kwargs):
        super(SectionForm, self).__init__(*args, **kwargs)
        self.user = current_user

    def save(self, commit=True, *args, **kwargs):

        obj = super(SectionForm, self).save(commit=False)
        if not obj.pk:
            obj.created_by = self.user
            obj.modified_by = self.user
        else:
            obj.modifier_by = self.user
        obj.slug = u'{0},{1}'.format(slugify(obj.title), obj.id)
        if commit:
            obj.save()

