from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import ListView, DetailView
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

from white.models import Project, Section, Photo
from accounts.models import CustomUser
from forms import ProjectForm, SectionForm
# Create your views here.


class ProjectCreate(CreateView):
    form_class = ProjectForm
    model = Project
    tempate_name = 'create_project.html'
    success_url = '/project/list/'


class ProjectList(ListView):
    model = Project
    tempate_name = 'project_list.html'


class ProjectUpdate(UpdateView):
    model = Project
    form_class = ProjectForm


class PhotoList(ListView):
    model = Photo


class PhotoCreate(CreateView):
    model = Photo


class SectionCreate(CreateView):
    form_class = SectionForm
    model = Section
    tempate_name = 'create_section.html'
    success_url = '/section/list/'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(SectionCreate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(SectionCreate, self).get_form_kwargs()
        kwargs.update({'current_user': self.request.user})
        return kwargs


class SectionList(ListView):
    model = Section


class SectionUpdate(UpdateView):
    model = Section


class CustomUserList(ListView):
    model = CustomUser


class CustomUserUpdate(UpdateView):
    model = CustomUser

