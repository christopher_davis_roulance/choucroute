from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

from views import ProjectCreate, ProjectList, ProjectUpdate
from views import SectionCreate, SectionList, SectionUpdate
from views import PhotoCreate, PhotoList
from views import CustomUserList, CustomUserUpdate

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'blacknwhite.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    #url(r'^/black/$', BlackHomeView.as_view(), name='home'),
    url(r'^project/add$', ProjectCreate.as_view(), name='add_project'),
    url(r'^project/list$', ProjectList.as_view(), name='project_list'),
    url(r'^project/update/(?P<pk>\d+)$', ProjectUpdate.as_view(), name='project_update'),

    url(r'^photo/add$', PhotoCreate.as_view(), name='add_photo'),
    url(r'^photo/list$', PhotoList.as_view(), name='photo_list'),

    url(r'^section/add$', SectionCreate.as_view(), name='add_section'),
    url(r'^section/list$', SectionList.as_view(), name='section_list'),
    url(r'^section/update/(?P<pk>\d+)$', SectionUpdate.as_view(), name='section_update'),

    url(r'^user/update/(?P<pk>\d+)$', CustomUserUpdate.as_view(), name='user_update'),
    url(r'^(?P<user_typology>\d+)/list/$', CustomUserList.as_view(), name='user_list')

)
