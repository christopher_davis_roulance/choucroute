# -*- coding: utf-8 -*-

USER_TYPOLOGY = (
    (10, 'Architect'),
    (20, 'Client'),
    (30, 'Contributor'),
)
