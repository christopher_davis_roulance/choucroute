from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

from views import AccountsProfileView, AccountsSignupView


urlpatterns = patterns(
    '',
    url(r'^signup/$', AccountsSignupView.as_view(), name='accounts_signup'),
    url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'accounts/accounts_login.html'}, name='accounts_login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='accounts_logout'),
    url(r'^profile/(?P<pk>\d+)$', AccountsProfileView.as_view(), name='accounts_profile'),
)
