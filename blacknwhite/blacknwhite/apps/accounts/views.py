from django.shortcuts import render

from models import CustomUser

from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import ListView, DetailView
from django.core.urlresolvers import reverse_lazy, reverse

# Create your views here.


class AccountsSignupView(CreateView):
    model = CustomUser
    template_name = 'accounts/accounts_signup.html'
    success_url = reverse_lazy('')


class AccountsProfileView(DetailView):
    model = CustomUser
    template_name = 'accounts/accounts_profile.html'
