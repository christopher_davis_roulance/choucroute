# -*- coding: utf-8 -*-

from django.db import models

from managers import CustomUserManager
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin

from constants import USER_TYPOLOGY


class CustomUser(AbstractBaseUser):
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False, verbose_name=u'Admin')
    seniority = models.PositiveSmallIntegerField(blank=True, null=True, verbose_name=u'Expérience')
    length_of_service = models.PositiveSmallIntegerField(blank=True, null=True, verbose_name=u'Ancienneté')
    site = models.URLField(blank=True, null=True, max_length=512)

    first_name = models.CharField(
        blank=True,
        null=True,
        max_length=512,
        verbose_name=u'Prénom'
    )

    last_name = models.CharField(
        blank=True,
        null=True,
        max_length=512,
        verbose_name=u'Nom'
    )

    company_name = models.CharField(
        blank=True,
        null=True,
        max_length=512,
        verbose_name=u'Raison social'
    )

    adress = models.TextField(
        blank=True,
        null=True,
        max_length=512,
        verbose_name=u'Adresse'
    )

    trade = models.CharField(
        blank=True,
        null=True,
        max_length=512,
        verbose_name=u'Corps de métier'
    )
    user_typology = models.PositiveSmallIntegerField(choices=USER_TYPOLOGY, default=10, verbose_name=u'User Typology')
    objects = CustomUserManager()

    USERNAME_FIELD = 'email'

    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

    @property
    def is_architect(self):
        "Is the user an architect?"
        return self.user_typology == 10

    @property
    def is_client(self):
        "Is the user a client?"
        return self.user_typology == 20

    @property
    def is_contributor(self):
        "Is the user a contributor?"
        return self.user_typology == 30

    def __unicode__(self):
        if self.is_client:
            return u"%s : %s -- %s %s" % (
                self.__class__.__name__,
                self.company_name,
                self.last_name,
                self.first_name
            )
        elif self.is_contributor:
            return u"%s : %s -- %s %s" % (
                self.trade,
                self.company_name,
                self.last_name,
                self.first_name
            )
        elif self.is_architect:
            return u"%s %s" % (self.last_name, self.first_name)

