from django.shortcuts import render
from django.views.generic import DetailView
from django.http import Http404
from django.utils.translation import ugettext as _

from models import Section


class WhiteHomeView(DetailView):
    model = Section
    slug_field = None
    context_object_name = None
    slug_url_kwarg = None
    pk_url_kwarg = None
    template_name = 'home.html'

    queryset = Section.objects.all()

    def get_object(self):
        queryset = self.queryset
        queryset = queryset.filter(constant_name='HOME')
        obj = queryset.get()
        return obj
