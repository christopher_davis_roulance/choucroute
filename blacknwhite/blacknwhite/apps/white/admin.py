from django.contrib import admin

from models import (
    Section,
    Project,
    Photo,
    Slideshow,
    ProjectToClient,
    ProjectToArchitect,
    ProjectToContributor
)


class BaseAdmin(admin.ModelAdmin):
    exclude = ('created_by', 'modified_by', 'slug')

    def _set_modified_and_created_by(self, request, obj):
        # created_by and modified_by cannot be updated by the user,
        # it needs to be done automatically, for both the main object and
        # its related objects
        if not obj.pk:
            obj.created_by = request.user
        obj.modified_by = request.user

    def save_model(self, request, obj, form, change):

        self._set_modified_and_created_by(request, obj)

        super(BaseAdmin, self).save_model(request, obj, form, change)


class SectionAdmin(BaseAdmin):
    raw_id_fields = ('primary_section', )


class ProjectToClientInline(admin.TabularInline):
    model = ProjectToClient
    raw_id_fields = ('client', )
    extra = 1


class ProjectToArchitectInline(admin.TabularInline):
    model = ProjectToArchitect
    raw_id_fields = ('architect', )
    extra = 1


class ProjectToContributorInline(admin.TabularInline):
    model = ProjectToContributor
    raw_id_fields = ('contributor', )
    extra = 1


class SlideshowInline(admin.TabularInline,):
    model = Slideshow
    raw_id_fields = ('photo', )
    extra = 1


class ProjectAdmin(BaseAdmin):
    raw_id_fields = ('section', )
    inlines = (
        ProjectToClientInline,
        ProjectToArchitectInline,
        ProjectToContributorInline,
        SlideshowInline
    )


class PhotoAdmin(BaseAdmin):
    pass


# Register your models here.
admin.site.register(Section, SectionAdmin)
admin.site.register(Photo, PhotoAdmin)
admin.site.register(Project, ProjectAdmin)
