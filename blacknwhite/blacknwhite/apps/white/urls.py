from django.conf.urls import patterns, include, url

from django.contrib import admin

from white.views import (
    WhiteHomeView,
#     WhiteSectionView,
#     WhiteContentView,
#     ContactView,
)

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', WhiteHomeView.as_view(), name='home'),
    # url(r'^/(?P<slug>\w+)/$', WhiteSectionView.as_view(), name='section-view'),
    # url(
    #     r'^/(?P<section>\w+)/(?P<slug>\w+)/$',
    #     WhiteContentView.as_view(),
    #     name='content-view'
    # ),
    # url(r'^$', ContactView.as_view(), name='contact'),
)
