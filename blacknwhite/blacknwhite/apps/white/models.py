# -*- coding: utf-8 -*-
from django.core.exceptions import ObjectDoesNotExist
from django.db import models

from accounts.models import CustomUser

from constant import WORKFLOW_STATE
# Create your models here.


class BaseModel(models.Model):
    title = models.CharField(max_length=512, verbose_name=u'Titre')
    WORKFLOW_STATE = WORKFLOW_STATE
    slug = models.CharField(blank=True, max_length=100)
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name=u"Date de création"
    )

    created_by = models.ForeignKey(
        CustomUser,
        related_name="%(class)s_creator_set",
        verbose_name=u"Créé par"
    )

    modified_at = models.DateTimeField(
        auto_now=True,
        verbose_name=u"Date de modification"
    )

    modified_by = models.ForeignKey(
        CustomUser,
        related_name="%(class)s_modificator_set",
        verbose_name=u"Modifié par"
    )

    workflow_state = models.PositiveSmallIntegerField(
        choices=WORKFLOW_STATE,
        default=10,
        verbose_name=u"Statut"
    )

    def __unicode__(self):
        return u"%s id : %d" % (self.title, self.pk)

    class Meta:
        abstract = True
        ordering = ['modified_at']


class Photo(BaseModel):

    credits = models.CharField(
        blank=True,
        max_length=100,
        verbose_name=u'Crédit affiché'
    )

    caption = models.CharField(
        blank=True,
        max_length=512,
        verbose_name=u'Légende'
    )

    original_file = models.ImageField(
        upload_to='photos/',
        max_length=200,
        verbose_name=u"Fichier image"
    )

    def __unicode__(self):
        return u"%s photo de %s" % (self.title, self.credits)


class Section(BaseModel):
    is_primary = models.BooleanField(default=False)
    constant_name = models.CharField(max_length=64)
    primary_section = models.ForeignKey(
        'self',
        blank=True,
        null=True,
        verbose_name=u"Rubrique parente"
    )

    def __unicode__(self):
        if self.primary_section:
            return u"%s dans %s" % (self.title, self.primary_section)
        return u"%s" % (self.title)

    def get_child_section(self):
        if self.is_primary:
            try:
                childs = Section.objects.filter(primary_section=self, workflow_state=20)
            except ObjectDoesNotExist:
                return
            return childs

    def get_auto_content(self):
        #as Home is not a manual section
        #we need to populate it with child section's content.

        childs = self.get_child_section()
        content = []
        for entry in childs:
            try:
                projects = Project.objects.all()[:3]
                content += projects
            except ObjectDoesNotExist:
                return

        return content


class Project(BaseModel):
    section = models.ForeignKey(
        Section,
        blank=True,
        null=True,
        verbose_name=u"Rubrique parente"
    )

    clients = models.ManyToManyField(
        CustomUser,
        related_name="projects_client",
        through=u'ProjectToClient'
    )

    contributors = models.ManyToManyField(
        CustomUser,
        related_name="projects_contributor",
        through=u'ProjectToContributor'
    )

    architects = models.ManyToManyField(
        CustomUser,
        related_name="projects_architect",
        through=u'ProjectToArchitect'
    )

    design_start_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name=u"beginning of conceptual design"
    )

    construction_start_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name=u"beginning of construction"
    )

    estimated_completion = models.DateTimeField(
        verbose_name=u"estimated completion"
    )

    completion = models.DateTimeField(
        auto_now_add=True,
        verbose_name=u"completion"
    )

    country = models.CharField(max_length=512, verbose_name=u'Pays')
    city = models.CharField(max_length=512, verbose_name=u'Ville')
    post_code = models.CharField(max_length=512, verbose_name=u'Code postal')
    site_area = models.PositiveIntegerField()
    built_area = models.PositiveIntegerField()
    maximum_height = models.IntegerField()
    program = models.TextField(verbose_name=u'programme')
    concept = models.TextField(verbose_name=u'Concepte')
    photos = models.ManyToManyField(Photo, through=u'Slideshow')

    def __unicode__(self):
        return u"%s : %s" % (self.section, self.title)


class ProjectToClient(models.Model):
    project = models.ForeignKey(Project)
    client = models.ForeignKey(CustomUser, limit_choices_to={'user_typology': 20})

    class Meta:
        unique_together = ('client', 'project')

    def __unicode__(self):
        return u"%s : %s" % (self.project, self.client)


class ProjectToContributor(models.Model):
    project = models.ForeignKey(Project)
    contributor = models.ForeignKey(CustomUser, limit_choices_to={'user_typology': 30})

    class Meta:
        unique_together = ('contributor', 'project')

    def __unicode__(self):
        return u"%s : %s" % (self.project, self.contributor)


class ProjectToArchitect(models.Model):
    project = models.ForeignKey(Project)
    architect = models.ForeignKey(CustomUser, limit_choices_to={'user_typology': 10})

    class Meta:
        unique_together = ('architect', 'project')

    def __unicode__(self):
        return u"%s : %s" % (self.project, self.architect)


class Slideshow(models.Model):
    project = models.ForeignKey(Project)
    photo = models.ForeignKey(Photo)

    def __unicode__(self):
        return u"%s : %s" % (self.project, self.photo)

